import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";class App extends Component {
    render() {
        return (
            <div>
                <Router>
                    <div>
                        <Route path="/" exact render={
                            () => {
                                return (<h1>Home page</h1>)
                            }
                        } />
                        <Route path="/contact" exact render={
                            () => {
                                return (<h1>Contact Page</h1>)
                            }
                        } />
                    </div>
                </Router>
            </div>
        );
    }
}export default App;
