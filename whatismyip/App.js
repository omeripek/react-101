import './App.css';
import {useState, useEffect} from 'react'
import axios from 'axios'

function App() {
  // Create IP state
  const [ip, setInfos] = useState('');

  // load ip from the API
  const getData = async () => {
    const res = await axios.get('https://geolocation-db.com/json/')
    setInfos(res.data.IPv4+' Country: '+ res.data.country_name)
  }

  useEffect( () => {
    // passing getData method to the lifecycle method
    getData()
  }, [])

  return (
    <div className="App">
      <h3>What is my IP?</h3>
      <h4>{ip}</h4>
    </div>
  );
}

export default App;
